# FIT4004 - Assignment 3 - Quick Start
David Copley, dncop1@student.monash.edu
Matt Battelic, mbat20@student.monash.edu

## Prerequisites
python >=3.5 installed
pip3 installed

## For a quick demonstration
```
pip3 install -r requirements.txt
chmod +x run.sh
./run.sh 
```
## Instructions
Example:
```
python3 ./src/main.py -a 37.8331119 -122.4834356 37.8280722 -122.4981393 -k 5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d -g F -n 3 -o "Filename.pdf"
```
Commands:
```
a <lat1> <long1> <lat2> <long2>
```
The search area, a box bounded by the points (lat1, long1), (lat2, long2) where lat1 and
lat2 are degrees of latitude specified by a signed floating point number, and long1 and long2
are degrees of longitude again specified by a floating point number. This command-line
argument is mandatory.
```
-k <access key>
```
This mandatory argument is to enter a Strava API access key. We specify this at the command
line so that we can test your program without having to use your API key!
```
-g <"M/F">
```
This optional command-line argument filters the search by gender. If not specified use the
fastest times regardless of gender.
```
-n <number of segments to display>
```
Display this number of segments. If not specified, default to 5. Output an error if this number is
less than 1 or greater than 20, or the argument is not a number.
```
-o <output file>
```
The name of the file to save the chart in. By default, save to out.pdf (in PDF format) in the
current directory.
