import requests

import argparse

from src.RestfulApiCaller import RestfulApiCaller
from src.StravaGraphFactory import StravaGraphFactory
from src.StravaService import StravaService


class ArgumentParser:
    '''
    parses arguments provided to program
    '''
    @staticmethod
    def getParser():
        '''
        instantiates a parser object
        :return: parser object
        '''
        parser = argparse.ArgumentParser()
        parser.add_argument('-a', nargs=4, help='<lat1> <long1> <lat2> <long2>')
        parser.add_argument('-k', help='<access key>')
        parser.add_argument('-g', help='<"M/F">')
        parser.add_argument('-n', help='<number of segments to display>')
        parser.add_argument('-o', help='<output file>')

        return parser

    @staticmethod
    def parseArguments(args):
        '''

        :param args:
        :return:
        '''
        bounds, access_key, gender, numSegments, outputFile = args.a, args.k, args.g, args.n, args.o

        assert bounds is not None and len(
            bounds) == 4, "Bounds is mandatory and must have 4 numbers, use -a <lat1> <long1> <lat2> <long2>"
        for bound in bounds:
            assert float(bound) == 0 or float(
                bound), "Bounds is mandatory and must have 4 numbers, use -a <lat1> <long1> <lat2> <long2>"

        assert access_key is not None, "Access Key is mandatory, use -k <access key>"
        assert numSegments is None or 0 < int(
            numSegments) <= 20, "Number of segments must be within range 1 - 20 inclusive"
        assert gender is None or gender == "M" or gender == "F", "Gender must be unspecified, 'M' or 'F'"
        if outputFile is not None:
            assert len(outputFile) > 0,'Invalid file name'
        if numSegments:
            numSegments = int(numSegments)
        return [float(bound) for bound in bounds], access_key, gender, numSegments, outputFile


if __name__ == "__main__":
    parser = ArgumentParser()

    args = parser.getParser().parse_args()
    bounds, access_key, gender, numSegments, outputFile = parser.parseArguments(args)

    clientSession = requests.Session()
    restfulApiCaller = RestfulApiCaller(clientSession)

    service = StravaService(access_key, restfulApiCaller)
    graphFactory = StravaGraphFactory()

    exploreSegments = service.getExploreSegments(bounds)
    segmentNames = service.getSegmentNames(exploreSegments)
    segmentIds = service.getSegmentIds(exploreSegments)
    segmentLeaderboards = [service.getSegmentLeaderboard(segmentId, gender) for segmentId in segmentIds]
    segmentMovingTimes = [service.getFastestMovingTimeForSegment(leaderboard) for leaderboard in segmentLeaderboards]
    movingTimesSegmentNamesTuples = service.createMovingTimesSegmentNamesTuples(segmentNames, segmentMovingTimes)

    graphFactory.plotSegmentsFastestTimesAchievedGraph(movingTimesSegmentNamesTuples, save=True, filename=outputFile,
                                                       numSegments=numSegments)
