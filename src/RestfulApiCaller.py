import urllib

class RestfulApiCaller:
    '''
    Acts as an abstraction layer on a lower level http request caller
    '''

    def __init__(self, clientSession):
        '''
        Constructor method for RestfulApiCaller class
        :param clientSession: instance of a http request object; should be able to make a get request through url string
        '''
        self.clientSession = clientSession

    def get(self, url, params):
        '''
        Http get request method
        :param url: the base url of api
        :param params: queries required for api call
        :return: response object if request is successful
        '''
        assert type(url) == str, "invalid URL type"
        assert params is None or type(params) == dict, "invalid URL type"
        if params is not None:
            urlEncodedParams = urllib.parse.urlencode(params)
        else:
            urlEncodedParams = ""
        res = self.clientSession.get(url + urlEncodedParams)
        assert res.status_code == 200, res.json()['message']
        return res.json()