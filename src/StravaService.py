import os
import matplotlib as mpl

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

import pandas as pd

class StravaService:
    '''
    Class acts as an abstraction of APIs made available by Strava
    '''

    def __init__(self, accessToken, restfulApiCaller):
        '''
        constructor for StravaService class
        :param accessToken: api access_token provided by Strava
        :param restfulApiCaller: an instance of RestfulApiCaller class
        '''
        self.accessToken = accessToken
        self.restfulApiCaller = restfulApiCaller

    def getExploreSegments(self, bounds):
        '''
        makes api call to get explore segments given geolocation boundaries
        :param bounds: two geolocation coordinates in the form of a array with length 4, should be valid coordinates
        :return: the segmentsJson response from the api call
        '''
        assert len(bounds) == 4, 'invalid number of bounds provided'
        for el in bounds:
            assert type(el) == float or type(el) == int, "invalid type of bounds provided"
        lat1, lon1, lat2, lon2 = bounds
        assert -90 <= lat1 <= 90, "invalid coordinate provided"
        assert -90 <= lat2 <= 90, "invalid coordinate provided"
        assert -180 <= lon1 <= 180, "invalid coordinate provided"
        assert -180 <= lon2 <= 180, "invalid coordinate provided"

        url = "https://www.strava.com/api/v3/segments/explore?"
        params = {'bounds': bounds, 'access_token': self.accessToken}
        segmentsJson = self.restfulApiCaller.get(url, params)
        return segmentsJson

    @staticmethod
    def getSegmentNames(segmentsJson):
        '''
        Extracts segment names from segmentsJson
        :param segmentsJson: json returned form explore segments call
        :return: a list of segment names
        '''
        assert 'segments' in segmentsJson, "missing key 'segments"
        if len(segmentsJson['segments']) == 0:
            return []
        segmentNames = pd.DataFrame.from_dict(segmentsJson['segments'])['name'].tolist()
        return segmentNames

    @staticmethod
    def getSegmentIds(segmentsJson):
        '''
        Extracts segment names from segmentsJson
        :param segmentsJson: json returned form explore segments call
        :return:a list of segment id
        '''
        assert 'segments' in segmentsJson, "missing key 'segments"
        if len(segmentsJson['segments']) == 0:
            return []
        segmentIds = pd.DataFrame.from_dict(segmentsJson['segments'])['id'].tolist()
        return segmentIds

    def getSegmentLeaderboard(self, segmentId, gender=None):
        '''
        makes api call to get the leaderboard of a given segment id
        :param segmentId: integer indicating segment id
        :param gender: 'M' for male, 'F' for female, or both if not provided
        :return: returns leaderboard json
        '''
        assert type(segmentId) == int, "segmentId must be of type int"
        assert gender is None or gender == "M" or gender == "F","Invalid gender provided"
        url = "https://www.strava.com/api/v3/segments/{}/leaderboard/?".format(segmentId)
        params = {'access_token': self.accessToken, 'per_page': '1'}
        if gender:
            params['gender'] = gender
        segmentLeaderboardJson = self.restfulApiCaller.get(url, params)

        return segmentLeaderboardJson

    @staticmethod
    def getFastestMovingTimeForSegment(segmentLeaderboardJson):
        '''
        extracts fastest moving time given a segment's leaderboard json
        :param segmentLeaderboardJson: leaderboard json returned by api call
        :return: integer indicating the fastest moving time
        '''
        fastestMovingTime = segmentLeaderboardJson['entries'][0]['moving_time']
        return fastestMovingTime

    @staticmethod
    def createMovingTimesSegmentNamesTuples(segmentNames, segmentMovingTimes):
        '''
        produces a sorted list of segmentNames,segmentMovingTimes tuples for producing graphs
        :param segmentNames: a list of strings or elements that can be converted to strings
        :param segmentMovingTimes: a list of integers of floats that aid plotting on graphs
        :return: a sorted list of segmentNames,segmentMovingTimes tuples
        '''
        assert len(segmentNames) == len(
            segmentMovingTimes), "number if elements in segments names and moving times must be equal"
        for el in segmentMovingTimes:
            assert (type(el) == int or type(el) == float) and el>=0 , "moving times must either be integers or floats greater than 0"
        movingTimesSegmentNamesTuples = []
        for segmentName, movingTime in zip(segmentNames, segmentMovingTimes):
            movingTimesSegmentNamesTuples.append((movingTime, segmentName))
        movingTimesSegmentNamesTuples.sort(reverse=True)
        return movingTimesSegmentNamesTuples