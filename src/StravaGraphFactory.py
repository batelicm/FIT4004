import os
import matplotlib as mpl

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

import matplotlib.pyplot as plt

class StravaGraphFactory:
    '''
    a class that produces different types of graphcs from Strava Data
    '''

    @staticmethod
    def plotSegmentsFastestTimesAchievedGraph(movingTimesSegmentNamesTuples, save=False, filename="out.pdf",
                                              numSegments=5):
        '''
        Plots "Fastest Times Achieved for the Longest n Segments in a Geographical Region" Graph
        :param movingTimesSegmentNamesTuples: a sorted list of segmentNames,segmentMovingTimes tuples
        :param save: boolean, indicating whether consumer wants graph to be saved or not
        :param filename: filename of saved graph
        :param numSegments: number of segments to show on graph, default to 5; if numSegments is greater than number of
         segments, will show all segments provided.
        :return: a matplotlib figure instance
        '''
        for el in movingTimesSegmentNamesTuples:
            assert type(el) == tuple, "Invalid element type in array of typles"
            assert type(el[0]) == int or type(el[0]) == float, "Invalid moving time type, must be int or float"
            assert type(el[1]) == str, "Invalid label type, must be string"
        if numSegments is None:
            numSegments = 5
        assert 0 < numSegments <= 20, "num segments must be within range (0,20]"
        tuples = movingTimesSegmentNamesTuples
        if len(movingTimesSegmentNamesTuples) > numSegments:
            tuples = movingTimesSegmentNamesTuples[:numSegments]
        if filename is None:
            filename = "out.pdf"
        else:
            assert len(filename) > 0, "filename must not be empty"
        index = range(len(tuples))
        f = plt.figure(figsize=(25, 5))
        plt.bar(range(len(tuples)), [tup[0] for tup in tuples])
        plt.xticks(index, [tup[1] for tup in tuples])
        plt.title("Fastest Times Achieved for the Longest n Segments in a Geographical Region")
        plt.ylabel("Moving Time")
        plt.xlabel("Segment Name")
        if save:
            f.savefig(filename, bbox_inches='tight')
        return f