# Test Strategy - David Copley, Matthew Batelic

# Introduction
The following test plan describes decisions and strategies made on developing test cases to test a software. The software in question is built as part of an assignment to demonstrate our knowledge in testable software, and furthermore demonstrate our ability to test testable software. We will incorporate OO knowledge and utilise continuous integration pipelines provided by Gitlab to show our ability to perform modern automated testing techniques.

The main functionality of the software is to produce a bar graph through provided arguments. The graph uses public APIs provided by Strava, a health related application that stores information on runners. These information pertain to the routes, distances of routes, time taken to complete the route, and basic information on users who have completed the routes. Details of the functionalities will be discussed in the following sections. 

Unit test has been written for functions of the software. Branch coverage of the unit tests is assessed to ensure they are testing adequate logical branches within the software. Assertions are intentionally included in the code to demonstrate the branch coverage of the written unit tests. CI pipelines have been setup on Gitlab; unit tests are automatically performed on each “push” to the master branch. 

It was a requirement that all functions that make network calls are mocked; we ensured this requirement was implemented.

For most functions we use equivalence partitioning, then boundary values for unit testing. Returned values of functions are tested through ‘valid’ inputs; we will discuss the definition of valid in the following section. 

# Classes, Functions and Test Strategies
The software is extremely simple. In its simplest form it does not require any classes. All functions use dependency injection; there is minimal coupling between them. We put the functions in logical classes for ease of testing. There are four main classes:
1. StravaService
2. StravaGraphFactory
3. RestfulApiCaller
4. Arguments Parser

## StravaService contains the following functions:
```
getExploreSegments(bounds)
```
This function takes bounds as its argument. Bounds is an array of four elements, each element must be an int or float. The elements represent two coordinates, each coordinate contains latitude and longitude; -90 <= latitude <=90; -180 <= longitude <= 180. 

For length of bounds there are two partitions, len(bounds) == 4, and len(bounds) != 4.
For type of element in bounds there are two partitions, type(el) is integer or float, and type(el) is neither float or int. 
For the latitudes, longitudes, each have three partitions: less than MIN, within MIN and MAX inclusive, and greater than MAX, where max are the values stated above. 
For each of the partitions stated above, the boundary values are tested, e.g. -90, -91 are tested for latitude; -180,-181 are tested for longitude; len(bounds) == 3 is tested; non-numbers are tested for type of elements.

Lastly, the value being returned is tested through “valid” inputs (those within the valid partition). 

The function contains a RestfulApiCaller instance that makes a network call; we test that class separately. (We mock the network call within the RestfulApiCaller object)

```
getSegmentNames(segmentsJson)
```
This function takes a JSON returned by the API in python dictionary form, extracts specific values from it through a given key, and returns it as an array. 

There are three partitions for the input: a valid JSON, an invalid JSON, or a JSON containing an empty array for segments. All three are tested. The returned value of the function is tested through ‘valid’ inputs, indifferent from the definition of ‘valid’ above. 

```
getSegmentIds(segmentsJson)
```
This function is the same as the getSegmentNames, except the key for extracting data is different; instead of extracting ‘names’, it extracts ‘ids’. The testing strategy is same as the one for getSegmentNames. 

```
getSegmentLeaderboard(segmentId, gender=None)
```
This function takes in one mandatory argument: segmentId, and one optional argument: gender. We assume segmentId must be an integer; there are two partitions: type(segmentId) == str and type(segmentId) != str. For gender there are four partitions: gender == None, gender == “M”, gender == “G”, and gender is none of the above. 

For this function we is combinatorial testing. We ensure all partitions are tested at least once. 

This functions, like getExploreSegments, makes a network call through an RestfulApiCaller instance. We test that separately. (We mock the network call within the RestfulApiCaller object)

```
getFastestMovingTimeForSegment(segmentLeaderboardJson)
```
The function takes one argument:segmentLeaderboardJson
There are two partitions: its either a valid segmentLeaderboardJson, or its not.

```
createMovingTimeSegmentNamesTuples(segmentNames, segmentMovingTimes))
```
This functions takes a list of segmentsNames and a list of segmentMoving times. 
For segmentNames, there are two partitions: segmentNames are strings, and segmentNames are not strings.
For segmentIds, there are two partitions: segmentIds are numbers(float or int), and are not numbers.
Furthermore, the two lists must be the same length. 

We assume that empty lists are valid, as we also assume that a bar graph that is empty (has no bars) is valid. 

## RestfulApiCaller handles all network http calls. It uses the ‘requests’ class. It contains the following function:
```
get(url, params)
```
The get function takes two arguments, url and params.
URL is mandatory and must be a string. There are two partitions for this argument: type(url) == str and not str. We will not validate the url; it is the request package’s job. 
Params is optional, it must be a dictionary. There are two partitions: type(params) == dict and not dict.

We mock the requests.Session instance in the class. There are two partitions for http responses: status_code == 200 and status_code != 200. We mock the response content and assume the backend produces correct results. 

We use combinatorial testing, ensuring each partition is tested at least once. As mentioned at the beginning, we test output of response through ‘valid’ inputs. 

## StravaGraphFactory handles the creation of graphs. It contains the following function:
```
plotSegmentsFastestTimesAchievedGraph
```

There is a parameter out of this assignment’s scope: save; its there for our convenience. We will not discuss about it.

The function takes the following arguments:
movingTimeSegmentNamesTuples - a list containing number(float or int), string pair tuples. There are three partitions: list elements are not tuples; first element in tuples are not numbers (int or float), and second element in tuples are not strings. We test all three partitions. 

filename - there are three partitions: filename is None, filename is a string with length greater than 1, and filename is neither the above cases. 

numSegments - there are five partitions: numSegments is None, numSegments is less than 1, numSegments is in range 1 and 20 inclusive, numSegments is greater than 20, or numSegments is none of the above cases, e.g. numSegments is of type string. 

We use combinatorial testing to test the above partitions. 

It is difficult to verify the graph is correct, we check that the value returned by matplotlin.pyplot is not None. 

We check whether a file has been created to test the save function; it is deleted afterwards. 

## ArgumentParser handles parsing of arguments. There are two functions:
```
getParser 
```
This function was intended to be a part of a larger ArugmentParser factory, but the idea was dropped. It simply returns an instance of argparse.ArgumentParser.

We test that an instance of ArgumentParser is returned. We also feed it with mocked arguments to ensure it is working. We attempted to further test invalid arguments, but found the error it produces (System Error) difficult to fix as it ties closely to the OS, so we instead tested the other function in the same class which has a similar function.

We mock the entry of arguments by using the argparse.Namespace class. 

```
parseArguments
```
this function parses each expected element, validates their type and their length, and throws an AssertionError if invalid. 

Arguments are mocked through the getParser function. 
The equivalent partitions are same as those mentioned above, the difference is instead of being passed as function arguments, they are passed through a Namespace object.

# Branch Coverage
```
Name                          Stmts   Miss  Cover   Missing
-----------------------------------------------------------
./src/RestfulApiCaller.py        13      1    92%   27
./src/StravaGraphFactory.py      32      3    91%   32, 36, 38
./src/StravaService.py           56      0   100%
./src/__init__.py                 0      0   100%
./src/main.py                    42     14    67%   56-74
-----------------------------------------------------------
TOTAL                           143     18    87%
```

We use the ‘coverage’ package to measure branch coverage. The branch coverage is more than 90%, ignoring the driver code that are run in ‘main.py’. This shows the test cases are effective in testing most branches within the code. 

Coverage reports are generated on every push as it is integrated with Gitlab CI. 
