import unittest
import os
import matplotlib as mpl

import sys
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

from src.main import StravaService, StravaGraphFactory, ArgumentParser, RestfulApiCaller
from unittest import mock
import json
import os
from argparse import Namespace
import requests

class IntegrationTest(unittest.TestCase):
    def setUp(self):
        with open("./test/mockData/exploreSegments.json") as f:
            self.mockExploreSegmentsData = json.loads(f.read().replace("'", '"'))
        with open("./test/mockData/segmentLeaderboard.json") as f:
            self.mockSegmentsLeaderboardData = f.read().split('|')
        self.mockSession = requests.Session()
        self.mockResponse = requests.Response()
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockExploreSegmentsData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)

    def testIntegration(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'F', '-n', '3', '-o',
                                  'unittest.pdf'])
        self.assertEqual(args, Namespace(a=['37.8331119', '-122.4834356', '37.8280722', '-122.4981393'], g='F',
                                         k='5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', n='3', o='unittest.pdf'))

        parsedArgs = ArgumentParser.parseArguments(args)
        self.assertEqual(([37.8331119, -122.4834356, 37.8280722, -122.4981393],
                          '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', 'F', 3, 'unittest.pdf'),
                         parsedArgs)

        exploreSegmentsData = self.strataService.getExploreSegments(
            [37.8331119, -122.4834356, 37.8280722, -122.4981393])
        self.assertEqual(exploreSegmentsData, self.mockExploreSegmentsData)

        segmentNames = self.strataService.getSegmentNames(self.mockExploreSegmentsData)
        self.assertEqual(segmentNames, ['Mount Lemmon', 'Cinderella Trail DH',
                                        'Palomar Mt South Grade (Taco shop to Summit Stop Sign)',
                                        'Chantry Flat from the Gate', 'San Bruno Hill Climb', 'Old La Honda - Mile 1'])

        segmentIds = self.strataService.getSegmentIds(self.mockExploreSegmentsData)
        self.assertEqual(segmentIds, [527881, 578907, 273807, 536957, 378701, 333801])

        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockSegmentsLeaderboardData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)
        segmentLeaderboard = self.strataService.getSegmentLeaderboard(123)
        self.assertEqual(segmentLeaderboard, self.mockSegmentsLeaderboardData)

        self.assertEqual(5937,
                         StravaService.getFastestMovingTimeForSegment(json.loads(self.mockSegmentsLeaderboardData[0])))

        tuples = self.strataService.createMovingTimesSegmentNamesTuples(["Hello", "World", "Foo", "Bar"],
                                                                        [22, 11, 31, 15])
        self.assertEqual(tuples, [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')])

        figure = StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
            movingTimesSegmentNamesTuples=tuples,
            save=True,
            filename="unittest"
        )
        self.assertIsNotNone(figure)
        self.assertEqual(os.path.isfile("./unittest.png"), True)
        if (os.path.isfile("./unittest.png")):
            os.remove("./unittest.png")

if __name__ == '__main__':
    unittest.main()