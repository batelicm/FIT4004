import unittest
import os
import matplotlib as mpl

import sys
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

from src.main import StravaService, StravaGraphFactory, ArgumentParser, RestfulApiCaller
from unittest import mock
import json
import os
from argparse import Namespace
import requests

class TestStravaService(unittest.TestCase):
    def setUp(self):
        with open("./test/mockData/exploreSegments.json") as f:
            self.mockExploreSegmentsData = json.loads(f.read().replace("'", '"'))
        with open("./test/mockData/segmentLeaderboard.json") as f:
            self.mockSegmentsLeaderboardData = f.read().split('|')
        self.mockSession = requests.Session()
        self.mockResponse = requests.Response()
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockExploreSegmentsData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)

    def testExploreSegmentsBounds_InBoundaryValues(self):
        exploreSegmentsData = self.strataService.getExploreSegments(
            [37.8331119, -122.4834356, 37.8280722, -122.4981393])
        self.assertEqual(exploreSegmentsData, self.mockExploreSegmentsData)

    def testExploreSegmentsBounds_OutOfBoundaryValuesLatitude(self):
        with self.assertRaises(AssertionError):
            self.strataService.getExploreSegments([-91, -122.4834356, 37.8280722, -122.4981393])

    def testExploreSegmentsBounds_OutOfBoundaryValuesLongitude(self):
        with self.assertRaises(AssertionError):
            self.strataService.getExploreSegments([-90, -180.4834356, 37.8280722, -122.4981393])

    def testExploreSegmentsBounds_OutOfBoundaryValuesBoundsArrayLength(self):
        with self.assertRaises(AssertionError):
            self.strataService.getExploreSegments([-90, -180.4834356, 37.8280722])

    def testExploreSegmentsBounds_OutOfBoundaryElementType(self):
        with self.assertRaises(AssertionError):
            self.strataService.getExploreSegments(["-90", -180.4834356, 37.8280722])

    def testExploreSegmentsBounds_InBoundaryIntegerElementType(self):
        exploreSegmentsData = self.strataService.getExploreSegments([1, 2, 3, 4])
        self.assertEqual(exploreSegmentsData, self.mockExploreSegmentsData)

    def testGetSegmentNames_InBoundaryValue(self):
        segmentNames = self.strataService.getSegmentNames(self.mockExploreSegmentsData)
        self.assertEqual(segmentNames, ['Mount Lemmon', 'Cinderella Trail DH',
                                        'Palomar Mt South Grade (Taco shop to Summit Stop Sign)',
                                        'Chantry Flat from the Gate', 'San Bruno Hill Climb', 'Old La Honda - Mile 1'])

    def testGetSegmentNames_OutOfBoundaryValue(self):
        segmentNames = self.strataService.getSegmentNames({"segments": []})
        self.assertEqual(segmentNames, [])

    def testGetSegmentNames_OutOfBoundaryValue_MissingKey(self):
        with self.assertRaises(AssertionError):
            self.strataService.getSegmentNames({})

    def testGetSegmentIds_InBoundaryValue(self):
        segmentIds = self.strataService.getSegmentIds(self.mockExploreSegmentsData)
        self.assertEqual(segmentIds, [527881, 578907, 273807, 536957, 378701, 333801])

    def testGetSegmentIds_OutOfBoundaryValue(self):
        segmentIds = self.strataService.getSegmentIds({"segments": []})
        self.assertEqual(segmentIds, [])

    def testGetSegmentIds_OutOfBoundaryValue_MissingKey(self):
        with self.assertRaises(AssertionError):
            self.strataService.getSegmentIds({})

    def testGetSegmentLeaderboard_InBoundaryValue_SegmentId(self):
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockSegmentsLeaderboardData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)
        segmentLeaderboard = self.strataService.getSegmentLeaderboard(123)
        self.assertEqual(segmentLeaderboard, self.mockSegmentsLeaderboardData)

    def testGetSegmentLeaderboard_OutOfBoundaryValue_SegmentId(self):
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockSegmentsLeaderboardData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)
        with self.assertRaises(AssertionError):
            self.strataService.getSegmentLeaderboard("123", 'M')

    def testGetSegmentLeaderboard_OutOfBoundaryValue_Gender(self):
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockSegmentsLeaderboardData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)
        with self.assertRaises(AssertionError):
            self.strataService.getSegmentLeaderboard("123", 'G')

    def testGetSegmentLeaderboard_InBoundaryValue_SegmentId_Gender(self):
        self.mockResponse.status_code = 200
        self.mockResponse.json = mock.Mock(return_value=self.mockSegmentsLeaderboardData)
        self.mockSession.get = mock.Mock(return_value=self.mockResponse)
        self.restfulApiCaller = RestfulApiCaller(self.mockSession)
        self.strataService = StravaService("ACCESS TOKEN", self.restfulApiCaller)
        segmentLeaderboard = self.strataService.getSegmentLeaderboard(123, "M")
        self.assertEqual(segmentLeaderboard, self.mockSegmentsLeaderboardData)

    def testGetFastestMovingTimeForSegment_InBoundaryValue(self):
        self.assertEqual(5937,StravaService.getFastestMovingTimeForSegment(json.loads(self.mockSegmentsLeaderboardData[0])))


    def testGetFastestMovingTimeForSegment_OutOfBoundaryValue(self):
        with self.assertRaises(Exception):
            StravaService.getFastestMovingTimeForSegment(str(json.loads(self.mockSegmentsLeaderboardData[0])))
    def testCreateMovingTimesSegmentNamesTuples_InBoundaryValue_names_movingTimesInt(self):
        tuples = self.strataService.createMovingTimesSegmentNamesTuples(["Hello", "World", "Foo", "Bar"],
                                                                        [22, 11, 31, 15])
        self.assertEqual(tuples, [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')])

    def testCreateMovingTimesSegmentNamesTuples_InBoundaryValues_names_movingTimesFloat(self):
        tuples = self.strataService.createMovingTimesSegmentNamesTuples(["Hello", "World", "Foo", "Bar"],
                                                                        [22.3, 11.1, 31.3, 15.5])
        self.assertEqual(tuples, [(31.3, 'Foo'), (22.3, 'Hello'), (15.5, 'Bar'), (11.1, 'World')])

    def testCreateMovingTimesSegmentNamesTuples_OutOfBoundaryValues_movingTimes(self):
        with self.assertRaises(AssertionError):
            self.strataService.createMovingTimesSegmentNamesTuples([1, 2.2, "Foo", "Bar"], [-22.3, 11.1, 31.3, 15.5])


class TestRestfulApiCaller(unittest.TestCase):
    def setUp(self):
        with open("./test/mockData/exploreSegments.json") as f:
            self.mockExploreSegmentsData = json.loads(f.read().replace("'", '"'))

        with open("./test/mockData//segmentLeaderboard.json") as f:
            self.mockSegmentsLeaderboardData = f.read().split('|')

    def testRestfulApiCaller_InBoundaryResponseStatusCode(self):
        mockSession = requests.Session()
        mockResponse = requests.Response()
        mockResponse.status_code = 200
        mockResponse.json = mock.Mock(return_value=self.mockExploreSegmentsData)
        mockSession.get = mock.Mock(return_value=mockResponse)
        restfulApiCaller = RestfulApiCaller(mockSession)
        response = restfulApiCaller.get("Url", {"Parameters": "Parameters"})
        self.assertEqual(response, self.mockExploreSegmentsData)

    def testRestfulApiCaller_OutOfBoundary_ResponseStatusCode(self):
        mockSession = requests.Session()
        mockResponse = requests.Response()
        mockResponse.status_code = 404
        mockResponse.json = mock.Mock(return_value={'message': 'Unable to connect to server.'})
        mockSession.get = mock.Mock(return_value=mockResponse)
        restfulApiCaller = RestfulApiCaller(mockSession)
        with self.assertRaises(AssertionError):
            restfulApiCaller.get("Url", {"Parameters": "Parameters"})

    def testRestfulApiCaller_OutOfBoundary_UrlType(self):
        mockSession = requests.Session()
        restfulApiCaller = RestfulApiCaller(mockSession)
        with self.assertRaises(AssertionError):
            restfulApiCaller.get(123, {"Parameters": "Parameters"})

    def testRestfulApiCaller_InBoundary_ParamTypeNone(self):
        mockSession = requests.Session()
        mockResponse = requests.Response()
        mockResponse.status_code = 200
        mockResponse.json = mock.Mock(return_value=self.mockExploreSegmentsData)
        mockSession.get = mock.Mock(return_value=mockResponse)
        restfulApiCaller = RestfulApiCaller(mockSession)
        response = restfulApiCaller.get("Url", {"Parameters": "Parameters"})
        self.assertEqual(response, self.mockExploreSegmentsData)

    def testRestfulApiCaller_OutOfBoundary_ParamTypeNotNoneOrDict(self):
        mockSession = requests.Session()
        mockResponse = requests.Response()
        mockResponse.status_code = 200
        mockSession.get = mock.Mock(return_value=mockResponse)
        restfulApiCaller = RestfulApiCaller(mockSession)
        with self.assertRaises(AssertionError):
            restfulApiCaller.get("Url", "params")


class TestStravaGraphFactory(unittest.TestCase):

    def testPlotSegmentsFastestTimesAchievedGraph_InBoundary_tupleList(self):
        movingTimeSegmentNamesTuplesList = [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        figure = StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
            movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList)
        self.assertIsNotNone(figure)

    def testPlotSegmentsFastestTimesAchievedGraph_InBoundary_saveIsTrue(self):
        movingTimeSegmentNamesTuplesList = [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        figure = StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
            movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
            save=True,
            filename="unittest"
        )
        self.assertIsNotNone(figure)
        self.assertEqual(os.path.isfile("./unittest.png"), True)
        if (os.path.isfile("./unittest.png")):
            os.remove("./unittest.png")

    def testPlotSegmentsFastestTimesAchievedGraph_InBoundary_saveIsTrue(self):
        movingTimeSegmentNamesTuplesList = [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        figure = StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
            movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
            save=True,
            filename="unittest"
        )
        self.assertIsNotNone(figure)
        self.assertEqual(os.path.isfile("./unittest.png"), True)
        if (os.path.isfile("./unittest.png")):
            os.remove("./unittest.png")

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_saveIsTrue_filenameEmpty(self):
        movingTimeSegmentNamesTuplesList = [(31, 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename=""
            )

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_InvalidTuplesList_StringForMovingTime(self):
        movingTimeSegmentNamesTuplesList = [("31", 'Foo'), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename="unittest"
            )

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_InvalidTuplesList_NoneForLabelName(self):
        movingTimeSegmentNamesTuplesList = [("31", None), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename="unittest"
            )

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_InvalidTuplesList_NonTuple(self):
        movingTimeSegmentNamesTuplesList = [[31, "Foo"], (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename="unittest"
            )

    def testPlotSegmentsFastestTimesAchievedGraph_InBoundaryBoundary_NumSegments(self):
        movingTimeSegmentNamesTuplesList = [(31, "Foo"), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        f = StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
            movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
            save=True,
            filename="unittest",
            numSegments=20
        )
        self.assertIsNotNone(f)

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_NumSegments_21(self):
        movingTimeSegmentNamesTuplesList = [(31, "Foo"), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename="unittest",
                numSegments=21
            )

    def testPlotSegmentsFastestTimesAchievedGraph_OutOfBoundaryBoundary_NumSegments_0(self):
        movingTimeSegmentNamesTuplesList = [(31, "Foo"), (22, 'Hello'), (15, 'Bar'), (11, 'World')]
        with self.assertRaises(AssertionError):
            StravaGraphFactory.plotSegmentsFastestTimesAchievedGraph(
                movingTimesSegmentNamesTuples=movingTimeSegmentNamesTuplesList,
                save=True,
                filename="unittest",
                numSegments=0
            )


class TestStravaArgumentParser(unittest.TestCase):
    def testGetParser(self):
        parser = ArgumentParser.getParser()
        self.assertIsNotNone(parser)

    def testParser_InBoundaryArguments(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'F', '-n', '3', '-o',
                                  'unittest.pdf'])
        self.assertEqual(args, Namespace(a=['37.8331119', '-122.4834356', '37.8280722', '-122.4981393'], g='F',
                                         k='5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', n='3', o='unittest.pdf'))

    def testParseArguments_InBoundaryArguments(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'F', '-n', '3', '-o',
                                  'unittest.pdf'])
        self.assertEqual(([37.8331119, -122.4834356, 37.8280722, -122.4981393],
                          '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', 'F', 3, 'unittest.pdf'),
                         ArgumentParser.parseArguments(args))

    def testParseArguments_OutOfBoundaryArguments_invalidBounds(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', 'bound1', 'bound2', 'bound3', 'bound4', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'F', '-n', '3', '-o',
                                  'unittest.pdf'])
        with self.assertRaises(Exception):
            ArgumentParser.parseArguments(args)

    def testParseArguments_OutOfBoundaryArguments_invalidGender(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'G', '-n', '3', '-o',
                                  'unittest.pdf'])
        with self.assertRaises(AssertionError):
            ArgumentParser.parseArguments(args)

    def testParseArguments_OutOfBoundaryArguments_invalidNumSegments_21(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'M', '-n', '21', '-o',
                                  'unittest.pdf'])
        with self.assertRaises(AssertionError):
            ArgumentParser.parseArguments(args)

    def testParseArguments_OutOfBoundaryArguments_invalidNumSegments_0(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'M', '-n', '0', '-o',
                                  'unittest.pdf'])
        with self.assertRaises(AssertionError):
            ArgumentParser.parseArguments(args)

    def testParseArguments_OutOfBoundaryArguments_invalidFileName(self):
        parser = ArgumentParser.getParser()
        args = parser.parse_args(['-a', '37.8331119', '-122.4834356', '37.8280722', '-122.4981393', '-k',
                                  '5a49c3ebf480b8a2f73c2543c4474a3ea8d6c78d', '-g', 'M', '-n', '1', '-o',
                                  ''])
        with self.assertRaises(AssertionError):
            ArgumentParser.parseArguments(args)


if __name__ == '__main__':
    unittest.main()
